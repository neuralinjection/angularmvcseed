﻿'use strict';

(function(document, undefined) {
    window.app = angular.module('app', ['ngResource', 'ui.bootstrap']);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider.
            when('/', {
                controller: 'HomeCtrl',
                templateUrl: '/'
            })
        .otherwise({ redirectTo: '/' });
        //$locationProvider.html5Mode(true);
    }]);

})(window.document)
