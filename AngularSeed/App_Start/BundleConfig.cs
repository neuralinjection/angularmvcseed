﻿using System.Web;
using System.Web.Optimization;

namespace AngularSeed
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true; 
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-*",
                        "~/Scripts/ui-bootstrap-*",
                        "~/Client/js/app.js",
                        "~/Client/js/controllers/HomeCtrl.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/bootstrap.min.css", "netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css").Include("~/Content/bootstrap/bootstrap.min.css"));
        }
    }
}